<?php


namespace App\Controller;


trait Helper
{

	/**
	 * Example:
	 *  round_up(12345.23, 1); // 12345.3
	 *  round_up(12345.23, 0); // 12346
	 *  round_up(12345.23, -1); // 12350
	 *  round_up(12345.23, -2); // 12400
	 *  round_up(12345.23, -3); // 13000
	 *  round_up(12345.23, -4); // 20000
	 *
	 * @param $value
	 * @param $places
	 * @return float|int
	 */
	public function round_up($value, $places)
	{
		$mult = pow(10, abs($places));
		return $places < 0 ?
			ceil($value / $mult) * $mult :
			ceil($value * $mult) / $mult;
	}
}