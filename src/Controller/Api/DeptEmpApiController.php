<?php

namespace App\Controller\Api;

use App\Controller\AbstractApiController;
use App\Entity\Department;
use App\Entity\DeptEmp;
use App\Entity\Employee;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/api/dept_emp")
 */
class DeptEmpApiController extends AbstractApiController implements BaseApiInterface
{
	private EntityManagerInterface $em;

	private const ENTITY_TYPE = 'dept_emp';

	public function __construct (EntityManagerInterface $em, string $apiToken)
	{
		$this->setApiToken('Bearer ' . md5($apiToken));
		$this->setIsTokenValid(false);
		$this->em = $em;

	}

	/**
	 * @Route("/get", name="api_dept_emp_get", methods={"GET"})
	 */
	public function getAction (Request $request): JsonResponse
	{
		switch ($this->isTokenValid($request)) {
			case Response::HTTP_UNAUTHORIZED:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_UNAUTHORIZED);
			case Response::HTTP_FORBIDDEN:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_FORBIDDEN);
			case Response::HTTP_OK:
				$this->setIsTokenValid(true);
				break;
		}
		$repository = $this->em->getRepository(DeptEmp::class);
		if ($request->get('id') !== null) {
			$entity = $repository->find($request->get('id'));
		} else {
			$entity = $repository->findAll();
		}

		if ($entity === null) {
			return $this->error(['message' => 'no matches with this id'], Response::HTTP_SERVICE_UNAVAILABLE);
		}

		$jsonData = $this->getDeptEmpJsonData($entity);
		return $this->success([
			'links' => 'http://localhost' . $request->getRequestUri(),
			'type' => self::ENTITY_TYPE,
			'method' => $request->getMethod(),
			'data' => $jsonData
		]);

	}

	/**
	 * @Route("/put", name="api_dept_emp_put", methods={"POST"})
	 */
	public function putAction (Request $request): JsonResponse
	{
		switch ($this->isTokenValid($request)) {
			case Response::HTTP_UNAUTHORIZED:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_UNAUTHORIZED);
			case Response::HTTP_FORBIDDEN:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_FORBIDDEN);
			case Response::HTTP_OK:
				$this->setIsTokenValid(true);
				break;
		}
		$formData = $request->get('form');
		$id = (int)$formData['id'];
		$entity = $this->em->getRepository(DeptEmp::class)->findOneBy(['id' => $id]);
		if ($entity === null) {
			return $this->error(['message' => 'no matches with this id'], Response::HTTP_SERVICE_UNAVAILABLE);
		}

		try {
			$this->creatDeptEmp($formData, $entity);
		} catch (\Exception $e) {
			return $this->error(['message' => $e->getMessage()], Response::HTTP_SERVICE_UNAVAILABLE);
		}
		return $this->success([
			'links' => 'http://localhost' . $request->getRequestUri(),
			'type' => self::ENTITY_TYPE,
			'method' => $request->getMethod(),
			'message' => 'Dept-Emp updated!'
		],
			Response::HTTP_CREATED);
	}

	/**
	 * @Route("/post", name="api_dept_emp_post", methods={"POST"})
	 */
	public function postAction (Request $request): JsonResponse
	{
		switch ($this->isTokenValid($request)) {
			case Response::HTTP_UNAUTHORIZED:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_UNAUTHORIZED);
			case Response::HTTP_FORBIDDEN:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_FORBIDDEN);
			case Response::HTTP_OK:
				$this->setIsTokenValid(true);
				break;
		}

		$formData = $request->get('form');

		try {
			$this->creatDeptEmp($formData);
		} catch (\Exception $e) {
			return $this->error(['message' => $e->getMessage()], Response::HTTP_SERVICE_UNAVAILABLE);
		}
		return $this->success([
			'links' => 'http://localhost' . $request->getRequestUri(),
			'type' => self::ENTITY_TYPE,
			'method' => $request->getMethod(),
			'message' => 'Dept-Emp created!'
		],
			Response::HTTP_OK);

	}

	/**
	 * @Route("/delete", name="api_dept_emp_delete", methods={"GET"})
	 */
	public function deleteAction (Request $request): JsonResponse
	{
		switch ($this->isTokenValid($request)) {
			case Response::HTTP_UNAUTHORIZED:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_UNAUTHORIZED);
			case Response::HTTP_FORBIDDEN:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_FORBIDDEN);
			case Response::HTTP_OK:
				$this->setIsTokenValid(true);
				break;
		}
		$repository = $this->em->getRepository(DeptEmp::class);

		try {
			$entity = $repository->find($request->get('id'));
			$this->em->remove($entity);
			$this->em->flush();
		} catch (\Exception $e) {
			return $this->error(['message' => $e->getMessage()], Response::HTTP_SERVICE_UNAVAILABLE);
		}

		$this->em->remove($entity);
		$this->em->flush();

		return $this->success([
			'links' => 'http://localhost' . $request->getRequestUri(),
			'type' => self::ENTITY_TYPE,
			'method' => $request->getMethod(),
			'message' => 'Dept-Emp deleted!'
		],
			Response::HTTP_OK);

	}

	private function getDeptEmpJsonData ($entity): array
	{
		$deptEmpJsonData = [];

		if ($entity instanceof DeptEmp) {
			$deptEmpJsonData = $this->getDeptEmpData($entity);
		} else {
			foreach ($entity as $deptEmp) {
				$deptEmpJsonData[] = $this->getDeptEmpData($deptEmp);
			}
		}

		return $deptEmpJsonData;
	}

	private function getDeptEmpData (DeptEmp $deptEmp): array
	{
		return [
			'empNo' => $deptEmp->getEmpNo()->getEmpNo(),
			'deptNo' => $deptEmp->getDeptNo()->getDeptNo(),
			'fromDate' => $deptEmp->getFromDate(),
			'toDate' => $deptEmp->getToDate(),
		];
	}

	private function creatDeptEmp ($formData, DeptEmp $deptEmp = null): void
	{
		if ($deptEmp === null) {
			$deptEmp = new DeptEmp();
		}

		$department = $this->em->getRepository(Department::class)->findOneBy(['deptNo' => (string)$formData['deptNo']]);
		$empNo = (int)$formData['empNo'];
		$employee = $this->em->getRepository(Employee::class)->findOneBy(['empNo' => $empNo]);

		$deptEmp->setEmpNo($employee);
		$deptEmp->setDeptNo($department);
		$deptEmp->setFromDate((new \DateTime($formData['fromDate'])));
		$deptEmp->setToDate((new \DateTime($formData['toDate'])));

		$this->em->persist($deptEmp);
		$this->em->flush();
	}
}