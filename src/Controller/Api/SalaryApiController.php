<?php

namespace App\Controller\Api;

use App\Controller\AbstractApiController;
use App\Entity\Employee;
use App\Entity\Salary;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/salary")
 */
class SalaryApiController extends AbstractApiController implements BaseApiInterface
{
	private EntityManagerInterface $em;

	private const ENTITY_TYPE = 'salary';

	public function __construct (EntityManagerInterface $em, string $apiToken)
	{
		$this->setApiToken('Bearer ' . md5($apiToken));
		$this->setIsTokenValid(false);
		$this->em = $em;

	}

	/**
	 * @Route("/get", name="api_salary_get", methods={"GET"})
	 */
	public function getAction (Request $request): JsonResponse
	{
		switch ($this->isTokenValid($request)) {
			case Response::HTTP_UNAUTHORIZED:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_UNAUTHORIZED);
			case Response::HTTP_FORBIDDEN:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_FORBIDDEN);
			case Response::HTTP_OK:
				$this->setIsTokenValid(true);
				break;
		}

		$repository = $this->em->getRepository(Salary::class);
		if ($request->get('id') !== null) {
			$entity = $repository->find($request->get('id'));
		} else {
			$entity = $repository->findAll();
		}

		if ($entity === null) {
			return $this->error(['message' => 'no matches with this id'], Response::HTTP_SERVICE_UNAVAILABLE);
		}

		$jsonData = $this->getSalaryJsonData($entity);
		return $this->success([
			'links' => 'http://localhost' . $request->getRequestUri(),
			'type' => self::ENTITY_TYPE,
			'method' => $request->getMethod(),
			'data' => $jsonData
		]);
	}

	/**
	 * @Route("/put", name="api_salary_put", methods={"POST"})
	 */
	public function putAction (Request $request): JsonResponse
	{
		switch ($this->isTokenValid($request)) {
			case Response::HTTP_UNAUTHORIZED:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_UNAUTHORIZED);
			case Response::HTTP_FORBIDDEN:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_FORBIDDEN);
			case Response::HTTP_OK:
				$this->setIsTokenValid(true);
				break;
		}
		$formData = $request->get('form');
		$id = (int)$formData['id'];
		$entity = $this->em->getRepository(Salary::class)->findOneBy(['id' => $id]);
		if ($entity === null) {
			return $this->error(['message' => 'no matches with this id'], Response::HTTP_SERVICE_UNAVAILABLE);
		}

		try {
			$this->creatSalary($formData, $entity);
		} catch (\Exception $e) {
			return $this->error(['message' => $e->getMessage()], Response::HTTP_SERVICE_UNAVAILABLE);
		}
		return $this->success([
			'links' => 'http://localhost' . $request->getRequestUri(),
			'type' => self::ENTITY_TYPE,
			'method' => $request->getMethod(),
			'message' => 'Salary updated!'
		],
			Response::HTTP_CREATED);
	}

	/**
	 * @Route("/post", name="api_salary_post", methods={"POST"})
	 */
	public function postAction (Request $request): JsonResponse
	{
		switch ($this->isTokenValid($request)) {
			case Response::HTTP_UNAUTHORIZED:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_UNAUTHORIZED);
			case Response::HTTP_FORBIDDEN:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_FORBIDDEN);
			case Response::HTTP_OK:
				$this->setIsTokenValid(true);
				break;
		}

		$formData = $request->get('form');

		try {
			$this->creatSalary($formData);
		} catch (\Exception $e) {
			return $this->error(['message' => $e->getMessage()], Response::HTTP_SERVICE_UNAVAILABLE);
		}
		return $this->success([
			'links' => 'http://localhost' . $request->getRequestUri(),
			'type' => self::ENTITY_TYPE,
			'method' => $request->getMethod(),
			'message' => 'Salary created!'
		],
			Response::HTTP_OK);

	}

	/**
	 * @Route("/delete", name="api_salary_delete", methods={"GET"})
	 */
	public function deleteAction (Request $request): JsonResponse
	{
		switch ($this->isTokenValid($request)) {
			case Response::HTTP_UNAUTHORIZED:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_UNAUTHORIZED);
			case Response::HTTP_FORBIDDEN:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_FORBIDDEN);
			case Response::HTTP_OK:
				$this->setIsTokenValid(true);
				break;
		}
		$repository = $this->em->getRepository(Salary::class);

		try {
			$entity = $repository->find($request->get('id'));
			$this->em->remove($entity);
			$this->em->flush();
		} catch (\Exception $e) {
			return $this->error(['message' => $e->getMessage()], Response::HTTP_SERVICE_UNAVAILABLE);
		}

		$this->em->remove($entity);
		$this->em->flush();

		return $this->success([
			'links' => 'http://localhost' . $request->getRequestUri(),
			'type' => self::ENTITY_TYPE,
			'method' => $request->getMethod(),
			'message' => 'Salary deleted!'
		],
			Response::HTTP_OK);

	}

	private function getSalaryJsonData ($entity): array
	{
		$salaryJsonData = [];

		if ($entity instanceof Salary) {
			$salaryJsonData = $this->getSalaryData($entity);
		} else {
			foreach ($entity as $salary) {
				$salaryJsonData[] = $this->getSalaryData($salary);
			}
		}

		return $salaryJsonData;
	}

	public function getSalaryData (Salary $salary): array
	{
		return [
			'salary' => $salary->getSalary(),
			'fromDate' => $salary->getFromDate(),
			'toDate' => $salary->getToDate(),
		];
	}

	private function creatSalary (array $formData, Salary $salary = null): void
	{
		if ($salary === null) {
			$salary = new Salary();
		}

		$empNo = (int)$formData['empNo'];
		$employee = $this->em->getRepository(Employee::class)->findOneBy(['empNo' => $empNo]);

		$salary->setEmpNo($employee);
		$salary->setFromDate((new \DateTime($formData['fromDate'])));
		$salary->setToDate((new \DateTime($formData['toDate'])));
		$salary->setSalary($formData['salary']);

		$this->em->persist($salary);
		$this->em->flush();
	}
}