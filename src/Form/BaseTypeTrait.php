<?php

namespace App\Form;

trait BaseTypeTrait
{
	public function getPastYearsAndCurrent(int $maxYear = 20): array
	{
		$currentYear = intval(\date('Y'));
		$retval = [];

		for ($i = 0; $i < $maxYear; $i++) {
			$retval[] = $currentYear - $i;
		}

		return $retval;
	}
}