<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields="email", message="Ez az e-mail használatban van")
 * @UniqueEntity(fields="username", message="Ez az username használatban van")
 */
class User implements UserInterface, \Serializable
{

	const ROLE_USER = 'ROLE_USER';
	const ROLE_ADMIN = 'ROLE_ADMIN';

	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	protected $id;

	/**
	 * @ORM\Column(type="string", length=50, unique=true, nullable=false)
	 * @Assert\NotBlank()
	 * @Assert\Length(min=4, max=50)
	 */
	protected $username;

	/**
	 * @ORM\Column(type="string")
	 */
	protected $password;

	/**
	 * @Assert\NotBlank()
	 * @Assert\Length(min=4, max=4096)
	 */
    protected $plainPassword;


	/**
	 * @ORM\Column(type="string", length=254, unique=true, nullable=false)
	 * @Assert\NotBlank()
	 * @Assert\Email()
	 */
	protected $email;

	/**
	 * @ORM\Column(type="string", length=50)
	 * @Assert\NotBlank()
	 * @Assert\Length(min=4, max=50)
	 */
	protected $fullName;

	/**
	 * @var array
	 * @ORM\Column(type="simple_array")
	 */
	protected $roles;

	public function getId(): ?int
    {
        return $this->id;
    }

	public function getRoles()
	{
		return $this->roles;
	}

	public function setRoles(array $roles): void
	{
		$this->roles = $roles;
	}

	public function getPassword()
	{
		return $this->password;
	}

	public function setPassword($password): void
	{
		$this->password = $password;
	}


	public function getSalt()
	{
		return null;
	}

	public function getUsername()
	{
		return $this->username;
	}

	public function setUsername(string $username): self
	{
		$this->username = $username;

		return $this;
	}


	public function eraseCredentials()
	{

	}

	public function serialize()
	{
		return serialize([
			$this->id,
			$this->username,
			$this->password
		]);
	}

	public function unserialize($serialized)
	{
		list($this->id,
			$this->username,
			$this->password) = unserialize($serialized);
	}

	public function getEmail()
	{
		return $this->email;
	}

	public function setEmail($email): void
	{
		$this->email = $email;
	}

	public function getFullName()
	{
		return $this->fullName;
	}

	public function setFullName($fullName): void
	{
		$this->fullName = $fullName;
	}

	public function getPlainPassword()
	{
		return $this->plainPassword;
	}

	public function setPlainPassword($plainPassword): void
	{
		$this->plainPassword = $plainPassword;
	}

}
