<?php

namespace App\Entity;

use App\Repository\DepartmentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=DepartmentRepository::class)
 * @ORM\Table(name="departments")
 * @UniqueEntity(fields="deptNo", message="This department id is in use")
 */
class Department
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer", name="dept_no")
	 */
    private $deptNo;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $deptName;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\DeptEmp", mappedBy="deptNo")
	 */
	private $deptEmps;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\DeptManager", mappedBy="deptNo")
	 */
	private $deptManagers;

	public function __construct()
	{
		$this->deptEmps = new ArrayCollection();
		$this->deptManagers = new ArrayCollection();
	}

    public function getDeptNo(): ?string
    {
        return $this->deptNo;
    }

    public function setDeptNo(string $deptNo): self
    {
        $this->deptNo = $deptNo;

        return $this;
    }

    public function getDeptName(): ?string
    {
        return $this->deptName;
    }

    public function setDeptName(string $deptName): self
    {
        $this->deptName = $deptName;

        return $this;
    }

	/**
	 * @return Collection|DeptEmp[]
	 */
	public function getDeptEmps(): Collection
	{
		return $this->deptEmps;
	}

	public function addDeptEmp(DeptEmp $deptEmp): self
	{
		if (!$this->deptEmps->contains($deptEmp)) {
			$this->deptEmps[] = $deptEmp;
			$deptEmp->setDeptNo($this);
		}

		return $this;
	}

	public function removeDeptEmp(DeptEmp $deptEmp): self
	{
		if ($this->deptEmps->removeElement($deptEmp)) {
			// set the owning side to null (unless already changed)
			if ($deptEmp->getDeptNo() === $this) {
				$deptEmp->setDeptNo(null);
			}
		}

		return $this;
	}


	/**
	 * @return Collection|DeptManager[]
	 */
	public function getDeptManagers(): Collection
	{
		return $this->deptManagers;
	}

	public function addDeptManager(DeptManager $deptManager): self
	{
		if (!$this->deptManagers->contains($deptManager)) {
			$this->deptManagers[] = $deptManager;
			$deptManager->setDeptNo($this);
		}

		return $this;
	}

	public function removeDeptManager(DeptManager $deptManager): self
	{
		if ($this->deptManagers->removeElement($deptManager)) {
			// set the owning side to null (unless already changed)
			if ($deptManager->getDeptNo() === $this) {
				$deptManager->setDeptNo(null);
			}
		}

		return $this;
	}
}
