<?php

namespace App\Entity;

use App\Repository\TitleRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=TitleRepository::class)
 * @ORM\Table(name="titles")
 * @UniqueEntity(fields="title", message="This title is in use")
 */
class Title
{

	use DateIntervalTrait;

	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private int $id;


	/**
	 * @ORM\Column(type="string", length=40, nullable=false, unique=true)
	 * @Assert\Length(min=1, max=40)
	 * @Assert\NotBlank()
	 */
	private string $title;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Employee", inversedBy="empNo")
     * @ORM\JoinColumn(nullable=false, name="emp_no", referencedColumnName="emp_no", onDelete="CASCADE")
     * @Assert\NotBlank()
     */
    private $empNo;

	public function getId ()
	{
		return $this->id;
	}

    public function getEmpNo(): ?employee
    {
        return $this->empNo;
    }

    public function setEmpNo(?employee $empNo): self
    {
        $this->empNo = $empNo;

        return $this;
    }

	public function getTitle ()
	{
		return $this->title;
	}

	public function setTitle ($title): self
	{
		$this->title = $title;
		return $this;
	}


}
