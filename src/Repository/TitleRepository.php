<?php

namespace App\Repository;

use App\Entity\Title;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Title|null find($id, $lockMode = null, $lockVersion = null)
 * @method Title|null findOneBy(array $criteria, array $orderBy = null)
 * @method Title[]    findAll()
 * @method Title[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TitleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Title::class);
    }

	public function getTitleCount(): ?int
	{
		$qb = $this->createQueryBuilder('t');

		return $qb->select('count(t)')
			->getQuery()
			->getSingleScalarResult();
	}

    // /**
    //  * @return Title[] Returns an array of Title objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Title
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
	public function getTitlesFromIntervals(int $limit = 10, int $offset = 0): ?array
	{
		$qb = $this->createQueryBuilder('t');

		return $qb->select('t')
			->orderBy('t.title', 'ASC')
			->setFirstResult( $offset )
			->setMaxResults( $limit )
			->getQuery()
			->getResult();
	}
}
