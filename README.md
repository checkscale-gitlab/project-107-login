# project-107-login

Login tesztfeladat

A 'main' repository -t használjuk ne a 'master' -t



#FONTOS - Olvass el:
A könnyebség kedvéért a leírásban xampp os illetve a docker es telepítési utmutatót is
bemutatom!

Figyelni az (docker / xampp ) esetekre!!

###Ami hiányzik részemről a kódból
- @SWG\Definition annotation az API hoz
- bővebb unitTest
- esetleg pár részt még kiszerveztem volna 
- illetve az adatbázis sémától eltértem mivel a doctrine 2 től problémásabb létrehozni az ilyen struktúrájú relációs adatbázisokat.. bízom benne így is lehet értékelni a munkámat
- dockerrel nem rég foglalkozom szóval kicsit fapados de jól configurálható szerintem 


---

## Környezeti, adatbázis betöltésével kapcsolatos tennivalók
Adatbázis telepítve van dockerben a generált migrations fájlok lefutatása (db műveletek)


###Domain
Docker esetén (előre deffiniálva van a .env file ban így javasolt ennek használata vagy átírása)

~~~
localhost:1107
~~~
Xampp esetén (a path / elnevezések csak szemléltetés)
###### win10 ..\etc\hosts
```
127.0.0.7 		localhost		project-107.local
```

###### win10 ..\apache\conf\extra\httpd-vhosts.conf
```
<VirtualHost *:80>
	DocumentRoot "c:/xampp/htdocs/project-107/public/"
	ServerName project-107.local
	<Directory "c:/xampp/htdocs/project-107/public/">
	</Directory>
</VirtualHost>
```

docker esetén (.env.docker szerint)
```
./run console doctrine:migrations:migrate -n
```
xampp esetén (.env.xampp szerint)

###### adatbázis kapcsolat generálása .env szerint
```
php bin/console doctrine:database:create
```
###### adatbázis migrations futtatása
```
php bin/console doctrine:migrations:migrate -n
```

###xampp esetén
composer telepítése.. install
~~~
composer install 
~~~


####Load Fixture és egyéb commandok
#####futtassa a projekt könyvtárában az alábbi commandot a dummy adatok betöltéséhez
docker esetén
```
./run console doctrine:fixtures:load -n
```
xampp esetén
```
php bin/console console doctrine:fixtures:load -n
```

Létrejött - Users
``` 
 -------------------------- -------- -------- ------------------------------------------
  email                      role     jelszó   username                                 
 -------------------------- -------- -------- ------------------------------------------
  teszt_elek@teszt.com       USER     qwertz2  teszt_elek     
  user@user.hu               USER     qwertz1  user     
  admin@admin.hu             ADMIN    qwertz   admin              
```
Létrejöttek továbbá a Employee, Title, Salary, Department és kapcsolói az Employee között.

#####futtassa a commandot könyvtárában új felhasználó generálásához
docker esetén
```
	./run console app:create-user app:create-user ize jelszo 0 ize@ize.hu "Ize Bigyó"
```
xampp esetén
```
	php bin/console app:create-user app:create-user ize jelszo 0 ize@ize.hu "Ize Bigyó"
```

#### Routes lekérése
~~~
php bin/console debug:router  
~~~
```
 -------------------------- ---------- -------- ------ ----------------------------------- 
Name                       Method     Scheme   Host   Path
 -------------------------- ---------- -------- ------ -----------------------------------

api_department_get         GET        ANY      ANY    /api/department/get
api_department_post        POST       ANY      ANY    /api/department/post
api_department_put         POST       ANY      ANY    /api/department/put
api_department_delete      GET        ANY      ANY    /api/department/delete
api_dept_emp_get           GET        ANY      ANY    /api/dept_emp/get
api_dept_emp_put           POST       ANY      ANY    /api/dept_emp/put
api_dept_emp_post          POST       ANY      ANY    /api/dept_emp/post
api_dept_emp_delete        GET        ANY      ANY    /api/dept_emp/delete
api_dept_manager_get       GET        ANY      ANY    /api/dept_manager/get
api_dept_manager_put       POST       ANY      ANY    /api/dept_manager/put
api_dept_manager_post      POST       ANY      ANY    /api/dept_manager/post
api_dept_manager_delete    GET        ANY      ANY    /api/dept_manager/delete
api_employee_get           GET        ANY      ANY    /api/employee/get
api_employee_post          POST       ANY      ANY    /api/employee/post
api_employee_put           POST       ANY      ANY    /api/employee/put
api_employee_delete        GET        ANY      ANY    /api/employee/delete
api_salary_get             GET        ANY      ANY    /api/salary/get
api_salary_put             POST       ANY      ANY    /api/salary/put
api_salary_post            POST       ANY      ANY    /api/salary/post
api_salary_delete          GET        ANY      ANY    /api/salary/delete
api_title_get              GET        ANY      ANY    /api/title/get
api_title_put              POST       ANY      ANY    /api/title/put
api_title_post             POST       ANY      ANY    /api/title/post
api_title_delete           GET        ANY      ANY    /api/title/delete
department_list            GET        ANY      ANY    /department/
department_create          GET|POST   ANY      ANY    /department/create
department_show            GET        ANY      ANY    /department/{deptNo}
department_update          GET|POST   ANY      ANY    /department/{deptNo}/update
department_delete          ANY        ANY      ANY    /department/delete/{deptNo}        
dept_emp_list              GET        ANY      ANY    /dept_emp/
project_index              ANY        ANY      ANY    /project
user_register              ANY        ANY      ANY    /register
salary_list                GET        ANY      ANY    /salary/
salary_create              GET|POST   ANY      ANY    /salary/create
salary_show                GET        ANY      ANY    /salary/{id}
salary_update              GET|POST   ANY      ANY    /salary/{id}/update
salary_delete              ANY        ANY      ANY    /salary/delete/{id}
security_login             ANY        ANY      ANY    /login
security_logout            ANY        ANY      ANY    /logout
title_list                 GET        ANY      ANY    /title/
title_create               GET|POST   ANY      ANY    /title/create
title_show                 GET        ANY      ANY    /title/{id}
title_update               GET|POST   ANY      ANY    /title/{id}/update
title_delete               ANY        ANY      ANY    /title/delete/{id}
user_update                ANY        ANY      ANY    /user/update/{username}
index                      ANY        ANY      ANY    /
 -------------------------- ---------- -------- ------ -----------------------------------
```

----
#Assets Instal
####ha [assets] nem működik megfelelően:
1.)
```assets
- composer install
```
2.) ellenőrizni kell a node, yarn fájlok meglétét majd
```assets
-yarn add @symfony/webpack-encore --dev
```
3.)
```assets
- yarn run encore dev
```
4.)
```assets
- php bin/console assets:install
```
*.)
```assets
yarn encore dev-server --hot
```


##Képernyőképek
kép: Főoldal (nem belépve)
        ![](public/readme-images/Képernyőkép%202021-12-29%20143847.jpg)

kép: Login
        ![](public/readme-images/Képernyőkép%202021-12-29%20143919.jpg)
        ![](public/readme-images/Képernyőkép%202021-12-29%20143953.jpg)

kép: Register
        ![](public/readme-images/Képernyőkép%202021-12-29%20144102.jpg)

kép: Register CLI
        ![](public/readme-images/Képernyőkép%202021-12-29%20144151.jpg)

kép: Főolddal (belépve)
        ![](public/readme-images/Képernyőkép%202021-12-29%20144015.jpg)

kép: Employee (list)
        ![](public/readme-images/Képernyőkép%202021-12-29%20144249.jpg)

kép: Employee (show)
        ![](public/readme-images/Képernyőkép%202021-12-29%20144307.jpg)

kép: Employee (update)
        ![](public/readme-images/Képernyőkép%202021-12-29%20144102.jpg)

kép: DeptEmp (show)
        ![](public/readme-images/Képernyőkép%202021-12-29%20144412.jpg)

kép: DeptEmp (update)
        ![](public/readme-images/Képernyőkép%202021-12-29%20144432.jpg)

kép: Api-key 
        ![](public/readme-images/use_api_key.jpg)

kép: Pstman (deptEm delete)
        ![](public/readme-images/postman_deptEmp_delete_access_1.jpg)

kép: PhpUnit Tests

![](public/readme-images/Képernyőkép%202021-12-29%20152830.jpg)
![](public/readme-images/Képernyőkép%202021-12-29%20152847.jpg)

kép: db uml 

![](public/readme-images/Képernyőkép%202021-12-29%20152518.jpg)

##Tesztelve az alábbi környezeteken
```
a) környezet docker
------------
php: 8.0
sql: 10.5-MariaDB
nginx

b) környezet Xampp 
------------
php: 7.4
mySql: 8
Apache: 2
```